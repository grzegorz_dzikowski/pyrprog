package zygrade.pyrprogram

import android.Manifest
import android.annotation.SuppressLint
import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.support.v4.app.ActivityCompat
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import zygrade.pyrprogram.AlarmBroadcastReceiver.Companion.notification
import zygrade.pyrprogram.quickperiodicjobscheduler.*
import java.lang.Thread.sleep
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

val mapper = ObjectMapper()

val url = "https://pastebin.com/raw/NL0jqxeT"

class MainActivity : AppCompatActivity() {

    data class FilterFilter(var piatek: Boolean = true, var sobota: Boolean = true, var niedziela: Boolean = true, var text: String = "", var nonstop: Boolean = true,
                            var acceptedBlock: MutableList<String> = mutableListOf(), var acceptedPlaces: MutableList<String> = mutableListOf())


    var aFilter = FilterFilter()

    var last = 0
    var sortByName = true

    private var viewAll = false

    private var currentView = CurrentView.MAIN

    companion object {
        val pyrkonTime = 1556265600000
        val dataFormat = SimpleDateFormat("dd:HH:mm:ss", Locale.GERMANY)
        val hourFormat = SimpleDateFormat("HH:mm", Locale.GERMANY)
        lateinit var listOfProgram: List<PyrkonEntry>
        lateinit var listOfFav: MutableList<String>

    }

    enum class CurrentView {
        MAIN, ITEM, FILTER, TIMER, IMAGE_VIEW
    }

    @SuppressLint("WrongViewCast")
    fun saveFilter() {
        aFilter.piatek = findViewById<CheckBox>(R.id.piatek).isChecked
        aFilter.sobota = findViewById<CheckBox>(R.id.sobota).isChecked
        aFilter.niedziela = findViewById<CheckBox>(R.id.niedziela).isChecked
        aFilter.nonstop = findViewById<CheckBox>(R.id.nonstop).isChecked
        aFilter.text = findViewById<EditText>(R.id.name).text.toString()
    }

    override fun setContentView(layoutResID: Int) {
        when (layoutResID) {
            R.layout.activity_main -> currentView = CurrentView.MAIN
            R.layout.item_show -> currentView = CurrentView.ITEM
            R.layout.filter -> currentView = CurrentView.FILTER
            R.layout.timer -> currentView = CurrentView.TIMER
            R.layout.image_view -> currentView = CurrentView.IMAGE_VIEW
        }
        super.setContentView(layoutResID)
        if (currentView == CurrentView.MAIN) {
            findViewById<TextView>(R.id.reklama).visibility = View.INVISIBLE
            val ad = findViewById<AdView>(R.id.adView)
            val req = AdRequest.Builder().build()
            ad.adListener = object : AdListener() {
                override fun onAdFailedToLoad(p0: Int) {
                    try {
                        ad.visibility = View.INVISIBLE
                        findViewById<TextView>(R.id.reklama).visibility = View.VISIBLE
                    } catch (ex: Exception) {
                        println("Ad FAILED")
                    }
                }
            }
            ad.loadAd(req)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE), 123)

        }
        startNotification()
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        MobileAds.initialize(this, "ca-app-pub-3753371031588243~3600487755")
        listOfProgram = mapper.readValue(resources.openRawResource(R.raw.program), mapper.typeFactory.constructCollectionType(List::class.java, PyrkonEntry::class.java))
        listOfProgram = listOfProgram.distinctBy { it.title }.sortedBy { it.title }
        val sh = getSharedPreferences("pyrkon_program", Context.MODE_PRIVATE)
        listOfFav = if (sh.contains("favs")) {
            mapper.readValue(sh.getString("favs", ""), mapper.typeFactory.constructCollectionType(MutableList::class.java, String::class.java))
        } else {
            mutableListOf()
        }
        setContentView(R.layout.activity_main)
        viewAll = listOfFav.isEmpty()
        loadValues(if (viewAll) listOfProgram.applyFilter() else listOfFav.applyFilterString())
    }

    private fun startNotification() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            val alarmMgr = applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val broadcastIntent = Intent(this, AlarmBroadcastReceiver::class.java)
            broadcastIntent.putExtra("requestAsService", true)
            val pIntent = PendingIntent.getBroadcast(this, 0, broadcastIntent, 0)

            alarmMgr.setInexactRepeating(
                    AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + 1000,
                    70000,
                    pIntent
            )
        } else {
            println("Job Manager")
            val job = QuickPeriodicJob(1234, object : PeriodicJob() {
                override fun execute(callback: QuickJobFinishedCallback) {
                    val i = Intent(applicationContext, AlarmBroadcastReceiver::class.java)
                    i.putExtra("requestAsService", false)
                    sendBroadcast(i)
                    callback.jobFinished()
                }

            })
            QuickPeriodicJobCollection.addJob(job)
            val q = QuickPeriodicJobScheduler(applicationContext)
            q.start(1234, 60000)
        }

    }


    override fun onBackPressed() {
        when (currentView) {
            CurrentView.MAIN -> System.exit(0)
            CurrentView.ITEM -> {
                setContentView(R.layout.activity_main)
                loadValues(if (viewAll) listOfProgram.applyFilter() else listOfFav.applyFilterString())
            }
            CurrentView.FILTER -> {
                setContentView(R.layout.activity_main)
                loadValues(if (viewAll) listOfProgram.applyFilter() else listOfFav.applyFilterString())
            }
            CurrentView.TIMER -> {
                timer.cancel()
                timer = Timer()
                setContentView(R.layout.activity_main)
                loadValues(if (viewAll) listOfProgram.applyFilter() else listOfFav.applyFilterString())
            }
            MainActivity.CurrentView.IMAGE_VIEW -> {
                setContentView(R.layout.activity_main)
                loadValues(if (viewAll) listOfProgram.applyFilter() else listOfFav.applyFilterString())
            }
        }
    }

    fun save() {
        getSharedPreferences("pyrkon_program", Context.MODE_PRIVATE).edit().putString("favs", mapper.writeValueAsString(listOfFav)).apply()
    }

    @SuppressLint("WrongViewCast")
    fun loadValues(l: List<PyrkonEntry>) {
        val list =
                (if (sortByName) l.sortedBy { it.title }
                else l.sortedBy { it.millis }).map { "${it.simple()} - ${it.title}" }
        findViewById<Button>(R.id.tools).setOnClickListener {
            AlertDialog.Builder(this).setTitle("Co chcesz otworzyć?").setPositiveButton("Mapa", { _: DialogInterface, _: Int ->
                setContentView(R.layout.image_view)
                findViewById<ImageView>(R.id.photo_view).setImageResource(R.drawable.map)
//                return@setPositiveButton
//                val intent = Intent()
//                val i = assets.open("img/image1.png");
//                val bitmap = BitmapFactory.decodeStream(i);
//                intent.action = android.content.Intent.ACTION_VIEW
//                getExternalFilesDir(Environment.DIRECTORY_PICTURES).mkdirs()
//                println(getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).path)
//                val f = File(getExternalFilesDir(null), "drawable/map.png")
//                println(f)
//                if(!f.exists()) {
//                    try {
//                        val out = FileOutputStream(f)
//                        assets.open("drawable/map.png").copyTo(out)
//                        out.close()
//                    }catch (ex: Exception)
//                    {
//                        Toast.makeText(this, "Error in opening the map: $ex. Contact the developer.", Toast.LENGTH_SHORT).show()
//                        ex.printStackTrace()
//                    }
//                }
//                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
//                    assets.open("drawable/map.png")
//                    intent.setDataAndType(Uri.fromFile(f), "image/png")
//                }
//                else{
//                    //intent.setDataAndType(bitmap, "image/png")
//                    println(intent.data)
//                }
//                startActivity(intent)
            }).setNeutralButton("Timer", { _: DialogInterface, _: Int ->
                setContentView(R.layout.timer)
                startTimer()
            }).setNegativeButton("Info", { _: DialogInterface, _: Int ->
                AlertDialog.Builder(this).setTitle("Info").setMessage("Twórca: Z-Grade#6423\nPodziękowania dla BCMike, Krypto i Search\nLogo: DevArt: sabrinaivanienko ")
                        .setPositiveButton("Discord Pycord") { _: DialogInterface, _: Int ->
                            val url = "https://discord.gg/vpA5wrJ"
                            val i = Intent(Intent.ACTION_VIEW)
                            i.data = Uri.parse(url)
                            startActivity(i)
                        }.setNegativeButton("Telegram", { _: DialogInterface, _: Int ->
                    val url = "https://t.me/Zgrate"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }).setNeutralButton("WWW Pyrkonu", { _: DialogInterface, _: Int ->
                    val url = "https://pyrkon.pl/"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }).show()
            }).show()
        }
        findViewById<Button>(R.id.sortby).setOnClickListener {
            AlertDialog.Builder(this).setTitle("Sortuj według").setPositiveButton("Nazwa", { _: DialogInterface, _: Int ->
                sortByName = true
                loadValues(if (viewAll) listOfProgram.applyFilter() else listOfFav.applyFilterString())
            }).setNegativeButton("Czas", { _: DialogInterface, _: Int ->
                sortByName = false
                loadValues(if (viewAll) listOfProgram.applyFilter() else listOfFav.applyFilterString())
            }).show()
        }
        if (list.isEmpty()) {
            val l = mutableListOf("Nie ma żadnych wyników :(")
            when (aFilter.text) {
                "zgrade" -> l.add("Z-Grade - Sleep room, Wioska Futrzaków")
                "krypto17" -> l.add("Ten to podrzuca nowe feature chociaż nie chce mi się ich pisać")
                "search" -> l.add("Pozdrówcie zielonego ropucha odemnie")
                "Furry OwO" -> l.add("UwU 'What's this?'")
                "Suicide" -> l.add("DON'T KILL YOURSELF!")
                "sfw" -> l.add("Serwer Futrzanych Meetów: https://discord.gg/fFBJzeh")
                "hug zgrade" -> l.add("*hug user, który uzywa aplikacji*")
                "reklama" -> l.add("Dziękuje za wsparcie!")
                "beep" -> l.add("Happy beeping :3")
                "sabrinaivanienko" -> l.add("Słodko czkasz :3")
            }
            findViewById<ListView>(R.id.list_1).adapter = ArrayAdapter<String>(this.applicationContext, R.layout.listrow, R.id.hiddenTextView, l)
            findViewById<ListView>(R.id.list_1).setOnItemClickListener { _: AdapterView<*>, _: View, _: Int, _: Long -> }
        } else {
            findViewById<ListView>(R.id.list_1).adapter = ArrayAdapter<String>(this.applicationContext, R.layout.listrow, R.id.hiddenTextView, list)
            val a = findViewById<View>(R.id.list_1) as ListView
            a.setSelection(last)
            a.setOnItemClickListener { adapterView: AdapterView<*>, view1: View, i: Int, l: Long ->
                last = i
                val title = (adapterView.getItemAtPosition(i) as String).let {
                    val split = it.split(" - ")
                    it.replaceFirst(split[0] + " - ", "")
                }
                val item = listOfProgram.find { it.title == title } ?: return@setOnItemClickListener
                setContentView(R.layout.item_show)
                findViewById<TextView>(R.id.title).text = item.title
                findViewById<TextView>(R.id.date).text = item.date
                findViewById<TextView>(R.id.duration).text = item.time
                findViewById<TextView>(R.id.blok).text = item.block
                findViewById<TextView>(R.id.miejsce).text = item.place
                findViewById<TextView>(R.id.opis).text = item.text
                val img = findViewById<ImageButton>(R.id.imageButton)
                if (listOfFav.contains(item.title)) {
                    img.setImageResource(R.drawable.yellow_star)
                } else {
                    img.setImageResource(R.drawable.star)
                }
                img.setOnClickListener {
                    if (!listOfFav.contains(item.title)) {
                        img.setImageResource(R.drawable.yellow_star)
                        listOfFav.add(item.title)
                        save()
                        Toast.makeText(this, "Dodano do ulubionych!", Toast.LENGTH_SHORT).show()
                    } else {
                        img.setImageResource(R.drawable.star)
                        listOfFav.remove(item.title)
                        save()
                        Toast.makeText(this, "Usunieto z ulubionych!", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        val show = findViewById<Button>(R.id.show_all)
        if (!viewAll) {
            show.text = "Pokaż Wszystkie"
        } else {
            show.text = "Pokaż Ulubione"
        }
        show.setOnClickListener {
            if (!viewAll) {
                show.text = "Pokaż Ulubione"
                viewAll = true
                loadValues(listOfProgram.applyFilter())
            } else {
                show.text = "Pokaż Wszystkie"
                viewAll = false
                loadValues(listOfFav.applyFilterString())
            }
        }
        findViewById<Button>(R.id.filter).setOnClickListener {
            setContentView(R.layout.filter)
            findViewById<CheckBox>(R.id.piatek).isChecked = aFilter.piatek
            findViewById<CheckBox>(R.id.sobota).isChecked = aFilter.sobota
            findViewById<CheckBox>(R.id.niedziela).isChecked = aFilter.niedziela
            findViewById<CheckBox>(R.id.nonstop).isChecked = aFilter.nonstop
            findViewById<EditText>(R.id.name).text.also { it.clear();it.insert(0, aFilter.text) }

            findViewById<Button>(R.id.chooseBlock).setOnClickListener {
                val alert = AlertDialog.Builder(this)
                alert.setTitle("Wybierz bloki")

                val blocks = listOfProgram.distinctBy { it.block }.map { it.block }.toTypedArray()
                if (aFilter.acceptedBlock.isEmpty()) {
                    aFilter.acceptedBlock.addAll(blocks)
                }
                alert.setMultiChoiceItems(blocks, blocks.map { aFilter.acceptedBlock.contains(it) }.toBooleanArray(), { _: DialogInterface, item: Int, check: Boolean ->
                    if (!check) {
                        aFilter.acceptedBlock.remove(blocks[item])
                    } else {
                        aFilter.acceptedBlock.add(blocks[item])
                    }
                })
                alert.setPositiveButton("Ok", { _, _ ->
                })
                alert.show()

            }
            findViewById<Button>(R.id.placeChoose).setOnClickListener {
                val alert = AlertDialog.Builder(this)
                alert.setTitle("Wybierz miejsca")

                val blocks = listOfProgram.distinctBy { it.place }.map { it.place }.toTypedArray()
                if (aFilter.acceptedPlaces.isEmpty()) {
                    aFilter.acceptedPlaces.addAll(blocks)
                }
                alert.setMultiChoiceItems(blocks, blocks.map { aFilter.acceptedPlaces.contains(it) }.toBooleanArray(), { _: DialogInterface, item: Int, check: Boolean ->
                    if (!check) {
                        aFilter.acceptedPlaces.remove(blocks[item])
                    } else {
                        aFilter.acceptedPlaces.add(blocks[item])
                    }
                })
                alert.setPositiveButton("Ok", { _, _ ->
                })
                alert.show()

            }

            findViewById<Button>(R.id.resetuj).setOnClickListener {
                aFilter = FilterFilter()
                findViewById<CheckBox>(R.id.piatek).isChecked = aFilter.piatek
                findViewById<CheckBox>(R.id.sobota).isChecked = aFilter.sobota
                findViewById<CheckBox>(R.id.niedziela).isChecked = aFilter.niedziela
                findViewById<CheckBox>(R.id.nonstop).isChecked = aFilter.nonstop
                findViewById<EditText>(R.id.name).text.also { it.clear();it.insert(0, aFilter.text) }
            }

            findViewById<Button>(R.id.accept).setOnClickListener {
                saveFilter()
                setContentView(R.layout.activity_main)
                loadValues(if (viewAll) listOfProgram.applyFilter() else listOfFav.applyFilterString())
            }
        }
    }

    var timer: Timer = object : Timer() {}
    private fun startTimer() {
        //println("WHAT THE HELLL?")
        timer.schedule(object : TimerTask() {
            @SuppressLint("WrongViewCast")
            override fun run() {
                //println("Spam")
                runOnUiThread {

                    val time = pyrkonTime - System.currentTimeMillis()
                    if (time < 0) {
                        findViewById<TextView>(R.id.pdate).text = "PYRKONNNNN!!!!!!!!!!"
                    } else {
                        findViewById<TextView>(R.id.pdate).text = getTime(time)
                    }
                }
            }

        }, 0L, 1000L)
    }

    fun List<String>.applyFilterString(): List<PyrkonEntry> {
        return this.toPyrkonEntry().applyFilter()
    }

    fun List<PyrkonEntry>.applyFilter(): List<PyrkonEntry> {
        return this.filter {
            it.day().allowed(filter = aFilter) && (it.title.contains(aFilter.text, ignoreCase = true) || it.text.contains(aFilter.text, ignoreCase = true)) && ((aFilter.acceptedBlock.isEmpty() || aFilter.acceptedBlock.contains(it.block))) && (aFilter.acceptedPlaces.isEmpty() || aFilter.acceptedPlaces.contains(it.place))
        }
    }

    fun List<String>.toPyrkonEntry(): List<PyrkonEntry> {
        return this.mapNotNull { t -> listOfProgram.find { it.title == t } }
    }
}

enum class Day {
    PIATEK, SOBOTA, NIEDZIELA, NONSTOP;

    fun allowed(filter: MainActivity.FilterFilter): Boolean {
        if (this == PIATEK && filter.piatek)
            return true
        if (this == SOBOTA && filter.sobota)
            return true
        if (this == NIEDZIELA && filter.niedziela)
            return true
        if (this == NONSTOP && filter.nonstop)
            return true
        return false
    }

}


fun PyrkonEntry.day(): Day {
    val a = this.date.split(", ")[0]
    return when (a) {
        "piątek" -> Day.PIATEK
        "sobota" -> Day.SOBOTA
        "niedziela" -> Day.NIEDZIELA
        "Non-stop" -> Day.NONSTOP
        else -> {
            println(a)
            TODO(a)
        }
    }
}


data class PyrkonEntry(var title: String = "", var url: String = "", var date: String = "", var time: String = "", var place: String = "",
                       var block: String = "", var text: String = "", var tags: List<String> = mutableListOf(), var millis: Long = 0)


fun getTime(time: Long): String {
    //val d = Calendar.getInstance().also { it.timeInMillis = time }
    val s = MainActivity.dataFormat.format(Date(time)).split(":")
    return "${s[0]} dni ${s[1]} godzin ${s[2]} minut ${s[3]} sekund"
}

fun getDay(time: Long): String {
    val s = MainActivity.dataFormat.format(Date(time)).split(":")
    return "${s[0]} dni"
}

fun PyrkonEntry.simple(): String {

    return "${when (day()) {
        Day.PIATEK -> "Pt"
        Day.SOBOTA -> "Sb"
        Day.NIEDZIELA -> "Ndz"
        Day.NONSTOP -> "N-stop"

    }} ${
    date.split(", ").let { if (it.size > 1) it[2] else "" }
    }"
}

class AlarmBroadcastReceiver(val requestAsIntent: Boolean = true) : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        // Create the notification to be shown
        MainActivity.listOfProgram = mapper.readValue(context.resources.openRawResource(R.raw.program), mapper.typeFactory.constructCollectionType(List::class.java, PyrkonEntry::class.java))
        MainActivity.listOfProgram = MainActivity.listOfProgram.distinctBy { it.title }.sortedBy { it.title }
        val sh = context.getSharedPreferences("pyrkon_program", Context.MODE_PRIVATE)
        MainActivity.listOfFav = if (sh.contains("favs")) {
            mapper.readValue(sh.getString("favs", ""), mapper.typeFactory.constructCollectionType(MutableList::class.java, String::class.java))
        } else {
            mutableListOf()
        }
        if (intent.getBooleanExtra("requestAsService", true)) {
            val i = Intent(context, BackendService::class.java)
            context.startService(i)
        } else {
            BackendService().downloadUpdates(context)
        }
        if (MainActivity.pyrkonTime - System.currentTimeMillis() > 0) {
            val b = Calendar.getInstance().also { it.timeInMillis = System.currentTimeMillis() }
            if (b.get(Calendar.HOUR_OF_DAY) == 12 && context.getSharedPreferences("pyrkon_program", Context.MODE_PRIVATE).getInt("day", 0) != b.get(Calendar.DAY_OF_MONTH)) {
                context.getSharedPreferences("pyrkon_program", Context.MODE_PRIVATE).edit().putInt("day", b.get(Calendar.DAY_OF_MONTH)).apply()
                notification(context, "Do pyrkonu pozostało ${getDay(MainActivity.pyrkonTime - System.currentTimeMillis())}!")
            }
        } else {
            val time = System.currentTimeMillis()
            val list = MainActivity.listOfFav.mapNotNull { f -> MainActivity.listOfProgram.find { it.title == f } }
            list.forEach {
                println("$time ${it.millis} ${it.millis - time}")
                if (it.millis != 0L) {

                    val diff = diffHourMin(it.millis, time)
                    println(diff.map { it })
                    if (diff[0] == 1 && diff[1] == 0) {
                        notification(context, "Przypomnienie: Za 1h masz event: ${it.title}! (${it.date} w ${it.place}!)")
                    } else if (diff[0] == 0 && diff[1] == 0) {
                        notification(context, "Event ${it.title} właśnie się zaczął! Odbywa się w ${it.place}!")
                    } else if (diff[0] == 2 && diff[1] == 0) {
                        notification(context, "Przypomnienie: Za 2h masz event: ${it.title}! (${it.date} w ${it.place}!)")
                    }
//                    when (diff[0]) {
//                        0L -> Unit
//                        in (3550000 until 3650000) -> notification(context, "Przypomnienie: Za 1h masz event: ${it.title}! (${it.date} w ${it.place}!)")
//                        in (0 until 60000) -> notification(context, "Event ${it.title} właśnie się zaczął! Odbywa się w ${it.place}!")
//
//                    }
                }
            }

        }

    }

    fun diffHourMin(t1: Long, t2: Long): IntArray {
        val diff = t1 - t2
        return intArrayOf(TimeUnit.MILLISECONDS.toHours(diff).toInt(), (TimeUnit.MILLISECONDS.toMinutes(diff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(diff))).toInt())
    }

    companion object {


        fun notification(context: Context, text: String) {
            val mBuilder = NotificationCompat.Builder(context, "my_app")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Pyrkon Program")
                    .setContentText(text)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setStyle(NotificationCompat.BigTextStyle().bigText(text))
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setVibrate(longArrayOf(0, 3000))
                    .setOnlyAlertOnce(true)

            val intent = Intent(context, MainActivity::class.java)
            val pending = PendingIntent.getActivity(context, 0, intent, 0)
            mBuilder.setContentIntent(pending)


            // Get the Notification manager service
            val am = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            // Generate an Id for each notification
            val id = System.currentTimeMillis() / 1000

            // Show a notification
            am.notify(id.toInt(), mBuilder.build())
        }
    }
}

class BackendService : IntentService("BackendService") {
    override fun onHandleIntent(p0: Intent) {
        downloadUpdates(applicationContext)
    }

    fun downloadUpdates(context: Context) {
        val times = mutableMapOf<Long, String>()
        try {
            val text = URL(url).readText()
            val a = applicationContext.getSharedPreferences("pyrkon_program", Context.MODE_PRIVATE).getStringSet("sended", mutableSetOf()).toMutableSet()
            println(a.toList())
            var modified = false
            text.split("\n").forEach {
                val t = it.split("\t")
                times += t[0].toLong() to t[1]
            }
            val currentTime = System.currentTimeMillis()
            times.filter { t -> !a.any { it == t.key.toString() } }.forEach {
                val diff = it.key - currentTime
                if (diff <= 0) {
                    modified = true
                    a.add(it.key.toString())
                    notification(context, it.value)
                    sleep(1000)
                }
            }
            if (modified) {
                applicationContext.getSharedPreferences("pyrkon_program", Context.MODE_PRIVATE).edit().putStringSet("sended", a).apply()
            }
        } catch (ex: Exception) {
            println("No Internet I guess....")
        }
    }
}